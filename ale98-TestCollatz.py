#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_2(self):
        s1= "150 50\n"
        i, j = collatz_read(s1)
        self.assertEqual(i, 150)
        self.assertEqual(j, 50)

    def test_read_3(self):
        s2 = "650000 800000\n"
        i, j = collatz_read(s2)
        self.assertEqual(i, 650000)
        self.assertEqual(j, 800000)

    def test_read_4(self):
        s3 = "999999 999999\n"
        i, j = collatz_read(s3)
        self.assertEqual(i, 999999)
        self.assertEqual(j, 999999)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 666, 540, 145)
        self.assertEqual(w.getvalue(), "666 540 145\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 770, 770, 34)
        self.assertEqual(w.getvalue(), "770 770 34\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 2000, 1200, 180)
        self.assertEqual(w.getvalue(), "2000 1200 180\n")

    def test_print_4(self):
        w = StringIO()
        collatz_print(w, 1, 999999, 525)
        self.assertEqual(w.getvalue(), "1 999999 525\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_2(self):
        r2 = StringIO("666 540\n770 770\n2000 1200\n1 999999\n")
        w2 = StringIO()
        collatz_solve(r2, w2)
        self.assertEqual(
            w2.getvalue(), "666 540 145\n770 770 34\n2000 1200 180\n1 999999 525\n")

    def test_solve_3(self):
        r3 = StringIO("320000 500000\n900000 800000\n100 250\n600000 300000\n")
        w3 = StringIO()
        collatz_solve(r3, w3)
        self.assertEqual(
            w3.getvalue(), "320000 500000 449\n900000 800000 525\n100 250 128\n600000 300000 470\n")

    def test_solve_4(self):
        r4 = StringIO("1 1000\n10000 100000\n179999 160000\n358000 358000\n")
        w4 = StringIO()
        collatz_solve(r4, w4)
        self.assertEqual(
            w4.getvalue(), "1 1000 179\n10000 100000 351\n179999 160000 370\n358000 358000 123\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
% coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


% coverage report -m                   >> TestCollatz.out



% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
